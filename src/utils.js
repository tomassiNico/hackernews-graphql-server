const jwt = require('jsonwebtoken')
const APP_SECRET = '4pr3nd13nd0GraphQLconM4ur1'

function getUserId(context){
  const Authorization = context.request.get('Authorization')
  if (Authorization){
    const token = Authorization.replace('Bearer ','')
    const { userId } = jwt.verify(token, APP_SECRET)
    return userId
  }

  throw new Error('No autorizado')
}

module.exports = {
  APP_SECRET, getUserId,
}
