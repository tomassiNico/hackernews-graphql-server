function user(root, args, context){
  return context.prisma.vote({ id: root.id }).user()
}

function link(root, args, context){
  return context.prisma.vote({ id: root.id }).link()
}

module.exports = {
  user, link, 
}
