const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const { APP_SECRET, getUserId } = require('../utils')

async function post(root, args, context, info){
  const userId = getUserId(context)
  return context.prisma.createLink({
    url: args.url,
    description: args.description,
    postedBy: { connect: { id: userId } },
  })
}


async function signup(root, args, context, info){
  // encripto la password pasada
  const password = await bcrypt.hash(args.password, 10)
  //creo el usuario con la password encriptada
  const user = await context.prisma.createUser({ ...args, password})
  //genero el token para devolverle al nuevo usuario
  const token = jwt.sign({ userId: user.id }, APP_SECRET)

  return {
    token,
    user,
  }
}


async function login(root, args, context, info){
  const user = await context.prisma.user({ email: args.email })
  if(!user){
    throw new Error('No se ha encontrado al usuario')
  }

  const valid = await bcrypt.compare(args.password, user.password)
  if(!valid){
    throw new Error('Contraseña inválida')
  }

  const token = jwt.sign({ userId: user.id }, APP_SECRET)

  return {
    token,
    user,
  }
}


async function vote(root, args, context, info){
  const userId = getUserId(context)

  const linkExists = await context.prisma.$exists.vote({
    user: { id: userId },
    link: { id: args.linkId },
  })
  if(linkExists){
    throw new Error(`Ya ha votado al link: ${args.linkId}`)
  }

  return context.prisma.createVote({
    user: { connect: { id: userId } },
    link: { connect: { id: args.linkId } },
  })
}

module.exports = {
  signup, login, post, vote
}
